﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Label : MonoBehaviour {

	//this variable is used to reference the position with the label
	public int index=0;
	//this variable is used to refer to the player's placing script
	LearningByPlacingLabels placingScript;
	// the buton used to trigger functions
	public Button theLabelButton;
	public Image arrow;


	// Use this for initialization
	void Start () {
		placingScript = GameObject.FindGameObjectWithTag ("player").GetComponent<LearningByPlacingLabels> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
		
	public void setLabel(GameObject go)
	{
		placingScript.selectedLabel = go;

	}


	//disable this script and set button to not interactive
	public void disable()
	{
		theLabelButton.interactable = false;
		arrow.enabled = false;

		this.enabled = false;
	

	}


	//just hide the arrow image
	public void disableSimple()
	{
		arrow.enabled = false;

	}



}
