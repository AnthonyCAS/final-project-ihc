﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	// Use this for initialization
	//this is the main score
	public int score;
	//these are the texts of each hud canvas
	public Text txt1,txt2;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void changeScore(int a)
	{
		score += a;
		txt1.text = "Score: " + score;
		txt2.text = "Score: " + score;

	}
}
