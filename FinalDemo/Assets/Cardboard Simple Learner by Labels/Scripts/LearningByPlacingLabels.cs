﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LearningByPlacingLabels : MonoBehaviour {

	// Use this for initialization

	// number of labels
	public int nbL=6;
	// temp value that stores the number of correct labels
	int nbCorrect=0;
	// this is the selected gameobject (the label)
	public GameObject selectedLabel;
	// this is the selected position (the position)
	public GameObject selectedPosition;
	// this is used to interpolate the movement of the label:
	Vector3 templabelPosition, tempPosPosition;
	GameObject tempLabel,tempPosition;

	//audio source to play sound according to the results:
	AudioSource audioS;
	// sound clips 
	public AudioClip audioOK,audioFail;
	// score script
	public Score myscore;

	//prefab of a flying score
	public GameObject flyingScoreUP,flyingScoreDW;
	// prefab of level finished!
	public GameObject levelFinished;

	void Start () {
		audioS = gameObject.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void checkLabel()
	{
		// get the indices of label and positions and check if it works. In case that they work:
		//   - deactivate scripts, -move to position, etc

		int indxL;
		int indxP;

		// perform actions only if both selections are ok!
		if(selectedLabel==null || selectedPosition==null)
		{
			return;
		}
		else
		{
			//this checks the index to set the label with its correct position
			indxL = selectedLabel.GetComponent<Label> ().index;
			indxP = selectedPosition.GetComponent<Position> ().index;
			templabelPosition = selectedLabel.transform.position;
			tempPosPosition=selectedPosition.transform.position;
			tempLabel = selectedLabel;
			tempPosition = selectedPosition;

			//sets to null the gameobjects (deselected) this prevents double-clicking effect
			selectedPosition=null;
			selectedLabel = null;
		}



		// in the case that the object was selected properly
		if (indxL == indxP) 
		{
			// use this line f you want instant positionnning
			//selectedLabel.transform.position = selectedPosition.transform.position;	



			// lerp motion of the label
			StartCoroutine(Move());
		} 
		else 
		{
			StartCoroutine(badMove());
		}

	}




	IEnumerator Move() {
		for (float f = 0f; f <=1; f += 0.03f) 
		{
			// interpolation using corrutine
			tempLabel.transform.position = (tempPosPosition- templabelPosition)*f+templabelPosition;	

			yield return null;
		}

		// play audio congratulations
		audioS.clip=audioOK;
		audioS.Play ();

		// add score
		myscore.changeScore(1);
		GameObject.Instantiate (flyingScoreUP, tempLabel.transform.position, tempLabel.transform.rotation);

		// increase correct labels
		nbCorrect+=1;

		// call end of level in the case of all labels placed
		if (nbCorrect == nbL) 
		{
            //levelFinished.SetActive (true);
            SceneManager.LoadScene(3, LoadSceneMode.Single);
        }


		// Fixed OK label to position:
		tempLabel.GetComponent<Label>().disable();
		tempPosition.GetComponent<Position> ().disable() ;
	

	}




	IEnumerator badMove() {
		for (float f = 0f; f <=1; f += 0.03f) 
		{
			// interpolation using corrutine
			tempLabel.transform.position = (tempPosPosition- templabelPosition)*f+templabelPosition;	

			yield return null;
		}
		tempLabel.GetComponent<Label>().disableSimple();	
		StartCoroutine_Auto (badMove2 ());

		// play audio congratulations
		audioS.clip=audioFail;
		audioS.Play ();

		// reduce score
		myscore.changeScore(-1);
		GameObject.Instantiate (flyingScoreDW, tempLabel.transform.position, tempLabel.transform.rotation);


	}

	IEnumerator badMove2() {
		for (float f = 0f; f <=1; f += 0.02f) 
		{
			// interpolation using corrutine
			tempLabel.transform.position = -(tempPosPosition- templabelPosition)*f+tempPosPosition;	

			yield return null;

		}

	}


}
