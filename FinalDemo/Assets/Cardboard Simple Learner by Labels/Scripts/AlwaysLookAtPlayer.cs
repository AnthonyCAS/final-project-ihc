﻿using UnityEngine;
using System.Collections;

public class AlwaysLookAtPlayer : MonoBehaviour {

	// Use this for initialization
	//we use this variable to refer to the transform of this gameobject
	Transform playerT;

	void Start () {

		playerT = GameObject.FindGameObjectWithTag ("player").transform;
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		transform.LookAt (2*transform.position- playerT.position);
	
	}
}
