﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Position : MonoBehaviour {

	// Use this for initialization
	//this variable is used to reference the position with the label
	public int index=0;
	// the buton used to trigger functions
	LearningByPlacingLabels placingScript;
	// this is the canvas used to make dissappear the position "i"
	public Canvas thisCanvas;

	void Start () {
		placingScript = GameObject.FindGameObjectWithTag ("player").GetComponent<LearningByPlacingLabels> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void setPosition(GameObject go)
	{
		placingScript.selectedPosition = go;
		placingScript.checkLabel ();
	}

	//disable this script and set button to not interactive
	public void disable()
	{
		thisCanvas.enabled = false;
		this.enabled = false;

	}


}
