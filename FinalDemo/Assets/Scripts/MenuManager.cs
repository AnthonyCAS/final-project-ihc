﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
 

public class MenuManager : MonoBehaviour {
    public Canvas MainMenu;
    public Canvas TypeSelector;
    public Canvas GameSelector;
    public Canvas SceneSelector;
    public GameObject[] Signs;
    // Use this for initialization
    void Start () {
        MainMenu.GetComponent<Canvas>().enabled =       true;
        TypeSelector.GetComponent<Canvas>().enabled =   false;
        GameSelector.GetComponent<Canvas>().enabled =   false;
        SceneSelector.GetComponent<Canvas>().enabled =  false;
        Signs[0].SetActive(false);
        Signs[1].SetActive(false);

    }
	
    public void OnMainMenuClick()
    {
        MainMenu.GetComponent<Canvas>().enabled = false;
        SceneSelector.GetComponent<Canvas>().enabled = true;
    }

    public void OnTypeSelectorClick()
    {
        TypeSelector.GetComponent<Canvas>().enabled = false;
        GameSelector.GetComponent<Canvas>().enabled = true;
        Signs[0].SetActive(true);
        Signs[1].SetActive(true);
    }

    public void OnGameSelectorClick()
    {
        GameSelector.GetComponent<Canvas>().enabled = false;
        SceneSelector.GetComponent<Canvas>().enabled = true;
        Signs[0].SetActive(false);
        Signs[1].SetActive(false);
    }

    public void OnSceneSelectorClick()
    {
        MainMenu.GetComponent<Canvas>().enabled = false;
        GameSelector.GetComponent<Canvas>().enabled = false;
        SceneSelector.GetComponent<Canvas>().enabled = true;
    }

    public void OnBathroomSceneClick()
    {
        SceneManager.LoadScene(5,LoadSceneMode.Single);
    }

    public void OnKitchenSceneClick()
    {
        SceneManager.LoadScene(3, LoadSceneMode.Single);
    }
    // Update is called once per frame
    void Update () {
	
	}
}
