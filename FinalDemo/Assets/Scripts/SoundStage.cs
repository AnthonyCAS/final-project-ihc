﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SoundStage : MonoBehaviour {
    public Canvas CardsMenu;
    AudioSource audioS;
    public AudioClip audioBaño, audioLavamanos, audioToalla, audioRepite;
    // Use this for initialization
    void Start () {
        audioS = gameObject.GetComponent<AudioSource>();
    }

    public void OnToallaClick()
    {
        audioS.clip = audioToalla;
        audioS.Play();
    }

    public void OnLavamanosClick()
    {
        audioS.clip = audioLavamanos;
        audioS.Play();
    }

    public void OnBanioClick()
    {
        audioS.clip = audioBaño;
        audioS.Play();
    }

    public void OnNextClick()
    {
        SceneManager.LoadScene(4, LoadSceneMode.Single);
    }
    // Update is called once per frame
    void Update () {
	
	}
}
