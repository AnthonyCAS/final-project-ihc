﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadOnClick : MonoBehaviour
{

    public GameObject loadingImage;

    public void LoadScene()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}