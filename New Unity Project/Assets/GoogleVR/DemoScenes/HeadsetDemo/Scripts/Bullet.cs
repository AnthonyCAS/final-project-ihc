﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
    public float speed = 2000.0f;
    public GameObject explosionPrefab;

    void OnCollisionEnter(Collision c)
    {
        // show an explosion
        // - transform.position because it should be
        //   where the rocket is
        // - Quaternion.identity because it should
        //   have the default rotation
        Instantiate(explosionPrefab,
                    transform.position,
                    Quaternion.identity);
        if (c.gameObject.tag == "destroyObject")
            Destroy(c.gameObject);
        // destroy the rocket
        // note:
        //  Destroy(this) would just destroy the rocket
        //                script attached to it
        //  Destroy(gameObject) destroys the whole thing
        Destroy(gameObject);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
