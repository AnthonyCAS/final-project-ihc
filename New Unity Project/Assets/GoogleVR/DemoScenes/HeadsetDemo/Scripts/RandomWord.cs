﻿using UnityEngine;
using System.Collections;

public class RandomWord : MonoBehaviour {
    string[] words = { "PERRO", "PERO", "PRO", "REO" };
    public string Word;
    public float timeLeft = 40;
    void Start ()
    {
        Word = RandomlyWord();
    }

    string RandomlyWord()
    {
        return words.RandomItem();
    }

	void Update () {
        GameObject.FindGameObjectWithTag("WordDisplay").GetComponent<UnityEngine.UI.Text>().text = Word;
        string wordget = GameObject.FindGameObjectWithTag("MessageDisplay").GetComponent<UnityEngine.UI.Text>().text;
        if (wordget == Word)
            GameObject.FindGameObjectWithTag("MessageDisplay").GetComponent<UnityEngine.UI.Text>().text = "";
        if(wordget != Word.Substring(0, wordget.Length))
            GameObject.FindGameObjectWithTag("MessageDisplay").GetComponent<UnityEngine.UI.Text>().text = "";
        Timer();        
	}

    void Timer()
    {
        timeLeft -= Time.deltaTime;
        if ( timeLeft < 0 )
        {
            Word = RandomlyWord();
            timeLeft = 40f;
        }
    }
}

public static class ArrayExtensions
{
    // This is an extension method. RandomItem() will now exist on all arrays.
    public static T RandomItem<T>(this T[] array)
    {
        return array[Random.Range(0, array.Length)];
    }
}