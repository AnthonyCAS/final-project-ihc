﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform player;               // Reference to the player's position.

    //PlayerHealth playerHealth;      // Reference to the player's health.
    //EnemyHealth enemyHealth;        // Reference to this enemy's health.
    //NavMeshAgent nav;               // Reference to the nav mesh agent.
    public float radius = 10f;
    public float speed = 1f;
    public bool offsetIsCenter = true;
    public float polePosition=0;
    public float speedFactor=0;
    public Vector3 offset;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if (offsetIsCenter)
        {
            offset = transform.position;
        }
    }

    void Update()
    {
        transform.position = new Vector3(
                    (radius * Mathf.Cos((Time.time + polePosition) * (speed + speedFactor))) + offset.x,
                    offset.y,
                    (radius * Mathf.Sin((Time.time + polePosition) * (speed + speedFactor))) + offset.z);
        if (player != null)
        {
            transform.LookAt(player);
        }
    }
        
}