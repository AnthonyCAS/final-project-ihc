﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RNGesus : MonoBehaviour {
    public Slider healthBarSlider;
    public GameObject[] Sequence;
    public Transform[] spawnPoints;
    private List<string> VSequence = new List<string>();
    public float timeLeft = 40f;
    public int Points=0;
    private int incomingPoints = 0;
    private int nextLevel;
    private Text[] textValue;
    public int lvl = 1;
    public GameObject retryButton;
    // Use this for initialization

    void Start () {
        Time.timeScale = 1;
        retryButton.SetActive(false);
        nextLevel = 3500 * lvl;
        healthBarSlider.maxValue = 3500 * lvl + 3500*(lvl-1);
        for (int i =0; i < Sequence.Length; i++)
        {
            GameObject aux = Sequence.RandomItem();
            VSequence.Add(aux.tag);
            aux.GetComponent<EnemyMovement>().radius = 400 + 150 * i;
            aux.GetComponent<EnemyMovement>().speedFactor = 0.5f * (lvl-1);
            Instantiate(aux, spawnPoints[i].position, spawnPoints[i].rotation);
        }
        GameObject canvas = GameObject.FindGameObjectWithTag("UICanvas");
        textValue = canvas.GetComponentsInChildren<Text>();
    }

    public void Initialize()
    {
        lvl = 1;
        Points = 0;
        incomingPoints = 0;
        timeLeft = 40;
    }
    public void EraseObject(GameObject c)
    {
        Debug.Log(c.GetComponent<Teleport>().enemyPoint);
        Points += c.GetComponent<Teleport>().enemyPoint;
        incomingPoints = c.GetComponent<Teleport>().enemyPoint;
        nextLevel -= incomingPoints;
        Destroy(c);
        healthBarSlider.value += c.GetComponent<Teleport>().enemyPoint;
        GameObject aux = Sequence.RandomItem();
        int rnNumber = (int)timeLeft % 3;
        aux.GetComponent<EnemyMovement>().radius = 400 + 150 * (int)Time.deltaTime % 3;
        aux.GetComponent<EnemyMovement>().polePosition = rnNumber;
        Instantiate(aux, spawnPoints[rnNumber].position, spawnPoints[rnNumber].rotation);
    }

    // Update is called once per frame
    void Update ()
    {
        //textValue[0].text = Points.ToString();
        //textValue[6].text = ""+(nextLevel);
        textValue[0].text = timeLeft.ToString("0");
        if(lvl > 3)
        {
            //textValue[6].text = "0";
            WinGame();
        }
        textValue[1].text = "Nivel "+ lvl;
        textValue[2].text = "Nivel " + (lvl+1);
        if (nextLevel <=0)
        {
            lvl += 1;
            timeLeft = 40f;
            Start();
        }

        //GameObject.FindGameObjectWithTag("timer").GetComponent<UnityEngine.UI.Text>().text = timeLeft.ToString("0");
        //GameObject.FindGameObjectWithTag("level").GetComponent<UnityEngine.UI.Text>().text = "Nivel: "+lvl;

        Timer();
    }

    void WinGame()
    {
        GameObject[] ddoge = GameObject.FindGameObjectsWithTag("Doge");
        GameObject[] dquack = GameObject.FindGameObjectsWithTag("Quack");
        GameObject[] dickey = GameObject.FindGameObjectsWithTag("Mickey");
        foreach (GameObject clone in ddoge)
        {
            Destroy(clone);
        }
        foreach (GameObject clone in dquack)
        {
            Destroy(clone);
        }
        foreach (GameObject clone in dickey)
        {
            Destroy(clone);
        }
        textValue[3].text = "GANASTE!!";
        retryButton.SetActive(true);
        Time.timeScale = 0;
    }
    void EndGame()
    {
        GameObject[] ddoge = GameObject.FindGameObjectsWithTag("Doge");
        GameObject[] dquack = GameObject.FindGameObjectsWithTag("Quack");
        GameObject[] dickey = GameObject.FindGameObjectsWithTag("Mickey");
        foreach (GameObject clone in ddoge)
        {
            Destroy(clone);
        }
        foreach (GameObject clone in dquack)
        {
            Destroy(clone);
        }
        foreach (GameObject clone in dickey)
        {
            Destroy(clone);
        }
        textValue[3].text = "PERDISTE";
        retryButton.SetActive(true);
        Time.timeScale = 0;
    }
    void Timer()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            EndGame();
        }
    }
}